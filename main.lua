-- Initialize project wide libraries
_G.gamestate_manager = require('libs.hump.gamestate')
_G.inspect = require('libs.inspect')

-- Preload gamestates to global table
_G.gamestates = {
    initial_background = require('src.states.InitialBackground'),
    menu = require('src.states.Menu'),
    game = require('src.states.Game')
}

function love.load()
    love.window.maximize()
    gamestate_manager.registerEvents()
    gamestate_manager.switch(gamestates.initial_background)
    gamestate_manager.switch(gamestates.menu)
end
