-- Load libraries
local ecs = require('src.ecs')

local engine = {
    avaliable_systems = require('src.engine.systems')
}

function engine:init()
    self.scenes = {}
end

function engine:load_scene(scene, reload)
    assert(type(scene) == "string", "Scene should be string")
    reload = reload or false

    if not reload == true then
        if self.scenes[scene] then
            error("Trying to load existing scene" .. scene)
        end
    end

    self.scenes[scene] = {
        conf = require('scenes.' .. scene),
        world = nil
    }
end

function engine:unload_scene(scene)
    assert(type(scene) == "string", "Scene should be string")

    if not self.scenes[scene] then
        error("Trying to unload not existing scene" .. scene)
    end

    table.remove(self.scenes, scene)
end

function engine:init_scene(scene)
    self.scenes[scene].world = ecs()

    for _, req_system in ipairs(self.scenes[scene].conf.systems) do
        if not self.avaliable_systems[req_system] then
            error("Unable to load requested system: " .. req_system)
        end

        self.scenes[scene].world:addSystem(self.avaliable_systems[req_system])
    end

    for _, component in ipairs(self.scenes[scene].conf.components) do
        if not component.name or not component.fields then
            error("Component must have name and fields")
        end
        self.scenes[scene].world:addComponent(component.name, component.fields)
    end

    for _, entity in ipairs(self.scenes[scene].conf.objects) do
        self.scenes[scene].world:addEntity(entity)
    end

    self.active_scene = scene
end

function engine:update(dt)
    self.world:emit("update", dt)
end

function engine:keypressed(key, scancode, isrepeat)
    self.world:emit("keypressed", key, scancode, isrepeat)
end

function engine:keyreleased(key, scancode)
    self.world:emit("keyreleased", key, scancode)
end

function engine:mousepressed(x, y, button, istouch, presses)
    self.world:emit("mousereleased", x, y, button, istouch, presses)
end

function engine:mousereleased(x, y, button, istouch, presses)
    self.world:emit("mousereleased", x, y, button, istouch, presses)
end

function engine:mousemoved(x, y, dx, dy, istouch)
    self.world:emit("mousemoved", x, y, dx, dy, istouch)
end

function engine:textinput(text)
    self.world:emit("textinput", text)
end

function engine:wheelmoved(x, y)
    self.world:emit("wheelmoved", x, y)
end

function engine:draw()
    self.world:emit("draw")
end

-- Development functions

function engine:addEntity(entity)
    return self.scenes[self.active_scene].world:addEntity(entity)
end

function engine:addSystem(system)
    return self.scenes[self.active_scene].world:addSystem(system)
end

function engine:addComponent(name, component)
    return self.scenes[self.active_scene].world:addComponent(name, component)
end

function engine:componentsFromEntity(entity)
    return self.scenes[self.active_scene].world:componentsFromEntity(entity)
end

function engine:getComponents(...)
    return self.scenes[self.active_scene].world:getComponents(...)
end

function engine:getComponentsFields(...)
    return self.scenes[self.active_scene].world:getComponentsFields(...)
end

function engine:hasComponent(entity, ...)
    return self.scenes[self.active_scene].world:hasComponent(entity, ...)
end

function engine:getEntities(...)
    return self.scenes[self.active_scene].world:getEntities(...)
end

function engine:emit(event, ...)
    return self.scenes[self.active_scene].world:emit(event, ...)
end

return engine