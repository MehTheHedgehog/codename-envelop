local inspect = require('libs.inspect')

local example_system = {}

function example_system:add(entity)
    print("Adding " .. inspect(entity))
end

return example_system