local engine = require('src.engine')

local Game = {}

function Game:init()
    engine:init()
    engine:init_scene()
end

function Game:enter(previous)
end

function Game:update(dt)
end

function Game:keypressed(key, scancode, isrepeat)
    engine:keypressed(key, scancode, isrepeat)
end

function Game:keyreleased(key, scancode)
    engine:keyreleased(key, scancode)
end

function Game:mousepressed(x, y, button, istouch, presses)
    engine:mousepressed(x, y, button, istouch, presses)
end

function Game:mousereleased(x, y, button, istouch, presses)
    engine:mousereleased(x, y, button, istouch, presses)
end

function Game:mousemoved(x, y, dx, dy, istouch)
    engine:mousemoved(x, y, dx, dy, istouch)
end

function Game:textinput(text)
    engine:textinput(text)
end

function Game:wheelmoved(x, y)
    engine:wheelmoved(x, y)
end

function Game:draw()
    engine:draw()
end

return Game