local nuklear = require('nuklear')

local MenuState = {}

function MenuState:init()
    self.ui = nuklear.newUI()
    self.height = (6 * 50) + 30
    self.width = 250
end

function MenuState:enter(previous)
    self.previous = previous
end

function MenuState:update(dt)
    self.ui:frameBegin()
    if self.ui:windowBegin('Overview',
                           (love.graphics.getWidth() - self.width) / 2,
                           (love.graphics.getHeight() - self.height) / 2,
                           self.width, self.height)
    then
        self.ui:layoutRow('dynamic', 50, 1)
        if self.ui:button('New Game') then
            gamestate_manager.switch(gamestates.game)
        end
        if self.ui:button('Load Game') then
            gamestate_manager.switch(gamestates.load_game)
        end
        if self.ui:button('Development Tools') then
            gamestate_manager.switch(gamestates.level_creator)
        end
        if self.ui:button('Options') then
            gamestate_manager.switch(gamestates.options)
        end
        if self.ui:button('About') then
            gamestate_manager.switch(gamestates.about)
        end
        if self.ui:button('Quit') then
            love.event.quit()
        end
        self.ui:windowEnd()
    end
    self.ui:frameEnd()
end

function MenuState:keypressed(key, scancode, isrepeat)
    self.ui:keypressed(key, scancode, isrepeat)
end

function MenuState:keyreleased(key, scancode)
    self.ui:keyreleased(key, scancode)
end

function MenuState:mousepressed(x, y, button, istouch, presses)
    self.ui:mousepressed(x, y, button, istouch, presses)
end

function MenuState:mousereleased(x, y, button, istouch, presses)
    self.ui:mousereleased(x, y, button, istouch, presses)
end

function MenuState:mousemoved(x, y, dx, dy, istouch)
    self.ui:mousemoved(x, y, dx, dy, istouch)
end

function MenuState:textinput(text)
    self.ui:textinput(text)
end

function MenuState:wheelmoved(x, y)
    self.ui:wheelmoved(x, y)
end

function MenuState:draw()
    self.previous:draw()

    -- Make background darker
    local oldR, oldG, oldB, oldA = love.graphics.getColor()
    love.graphics.setColor(0, 0, 0, 200 * (1 / 255))
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    love.graphics.setColor(oldR, oldG, oldB, oldA)

    self.ui:draw()
end

return MenuState