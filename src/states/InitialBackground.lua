local InitialBackground = {}

function InitialBackground:init()
    self.image = love.graphics.newImage('assets/images/menu-screen.jpg')
end

function InitialBackground:draw()
    love.graphics.draw(self.image, 0, 0, 0, 1, 1, 0, 0)
end

return InitialBackground