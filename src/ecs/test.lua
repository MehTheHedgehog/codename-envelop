describe("unit tests for Envelop ECS", function()
    before_each(function()
        _G.ecs = require('src.ecs.init')()
    end)

    it("test adding entity", function()
        local example_entity_1 = {}
        local example_entity_2 = {}

        ecs:addEntity(example_entity_1)
        ecs:addEntity(example_entity_2)

        assert.are.same(ecs.entities, {example_entity_1, example_entity_2})
        assert.are.same(ecs:getEntities(), {example_entity_1, example_entity_2})
    end)

    it("test adding components", function()
        ecs:addComponent("component1", {})
        ecs:addComponent("component2", {})

        assert.are.same(ecs.components, {component1 = {}, component2 = {}})
    end)

    it("test adding systems", function()
        local example_system_1 = {}
        local example_system_2 = {}

        assert.are.same(ecs.systems, {})

        ecs:addSystem(example_system_1)
        ecs:addSystem(example_system_2)

        assert.are.same(ecs.systems, {example_system_1, example_system_2})
    end)

    it("test if adding entity generate 'add' event", function()
        local example_system = {}
        stub(example_system, "add")
        ecs:addSystem(example_system)

        local example_entity = {}
        ecs:addEntity(example_entity)

        assert.stub(example_system.add).was.called(1)
        assert.stub(example_system.add).was.called_with(ecs, example_entity)
    end)

    it("test reliance between components and entities", function()
        local entity_1 = {
            x = 1,
            y = 2,
            z = 3
        }

        local entity_2 = {
            x = 2,
            y = 1
        }

        local entity_3 = {
            x = 3
        }

        local entity_4 = {
            z = 3
        }

        local position_component = {
            name = "position",
            fields = {"x", "y"}
        }

        local depth_component = {
            name = "depth",
            fields = {"z"}
        }

        ecs:addComponent(position_component.name, position_component.fields)
        ecs:addComponent(depth_component.name, depth_component.fields)

        ecs:addEntity(entity_1)
        ecs:addEntity(entity_2)
        ecs:addEntity(entity_3)
        ecs:addEntity(entity_4)

        assert.are.same({depth_component.fields, position_component.fields}, ecs:componentsFromEntity(entity_1))
        assert.are.same({position_component.fields}, ecs:componentsFromEntity(entity_2))
        assert.are.same({}, ecs:componentsFromEntity(entity_3))

        assert.are.same({entity_1, entity_2}, ecs:getEntities(position_component.name))
        assert.are.same({entity_1, entity_4}, ecs:getEntities(depth_component.name))
        assert.are.same({entity_1}, ecs:getEntities(depth_component.name, position_component.name))
    end)
end)