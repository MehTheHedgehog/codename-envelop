-- Got from https://github.com/kikito/memoize.lua/blob/bd7a8b8694649f1388d7b0e09ebda62ae036843f/memoize.lua#L37-L45
local function is_callable(f)
    local tf = type(f)
    if tf == 'function' then return true end
    if tf == 'table' then
        local mt = getmetatable(f)
        return type(mt) == 'table' and is_callable(mt.__call)
    end
    return false
end

-- ECS Implementation
local EntityComponentSystem = {}
EntityComponentSystem.__index = EntityComponentSystem

function EntityComponentSystem:addEntity(entity)
    if not self.entities.entity then
        table.insert(self.entities, entity)
        self:emit('add', self.entities[#self.entities])
    end
end

function EntityComponentSystem:addSystem(system)
    if not self.systems[system] then
        table.insert(self.systems, system)
    end
end

function EntityComponentSystem:addComponent(name, component)
    self.components[name] = component
end

function EntityComponentSystem:componentsFromEntity(entity)
    local components = {}
    for name, fields in pairs(self.components) do
        local has = true
        for _, field in pairs(fields) do
            if not entity[field] then
                has = false
                break
            end
        end
        if has then
            table.insert(components, fields)
        end
    end

    return components
end

function EntityComponentSystem:getComponents(...)
    local components = {}

    if arg.n == 0 or arg.n == nil then
        return self.components
    end

    for _, component in ipairs(arg) do
        if self.components[component] then
            components[components] = self.components[component]
        end
    end

    return components
end

function EntityComponentSystem:getComponentsFields(...)
    local fields = {}

    for _, component in ipairs(arg) do
        if self.components[component] then
            for _, field in pairs(self.components[component]) do
                table.insert(fields, field)
            end
        end
    end

    return fields
end

function EntityComponentSystem:_filterFields(object, ...)
    assert(object ~= nil, "Object should not be nil")

    for _, field in ipairs(arg) do
        if not object[field] then
            return false
        end
    end

    return true
end

function EntityComponentSystem:hasComponent(entity, ...)
    local fields_filter = self:getComponentsFields(...)

    return self:_filterFields(entity, unpack(fields_filter))
end

function EntityComponentSystem:getEntities(...)
    -- Return all entities in case of empty args
    if arg.n == 0 or arg.n == nil then
        return self.entities
    end

    local entities = {}
    local fields_filter = self:getComponentsFields(...)

    for _, entity in pairs(self.entities) do
        if self:_filterFields(entity, unpack(fields_filter)) then
            table.insert(entities, entity)
        end
    end

    return entities
end

function EntityComponentSystem:emit(event, ...)
    if self.systems then
        for _, system in ipairs(self.systems) do
            if is_callable(system[event]) then
                system[event](self, ...)
            end
        end
    end
end

return function ()
    local self = setmetatable({
        entities = {},
        systems = {},
        components = {}
    }, EntityComponentSystem)
    return self
end
